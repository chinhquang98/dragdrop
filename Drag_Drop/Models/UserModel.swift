//
//  UserModel.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/6/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import Foundation
import MobileCoreServices

final class UserModel: NSObject, NSItemProviderWriting, NSItemProviderReading, Codable {
    static var readableTypeIdentifiersForItemProvider: [String] {
        //We know we want to accept our object as a data representation, so we'll specify that here
        return [(kUTTypeData) as String]
    }
    static func object(withItemProviderData data: Data, typeIdentifier: String) throws -> UserModel {
        let decoder = JSONDecoder()
        do {
            //Here we decode the object back to it's class representation and return it
            let subject = try decoder.decode(UserModel.self, from: data)
            return subject
        } catch {
            fatalError(error as! String)
        }
    }
    
    static var writableTypeIdentifiersForItemProvider: [String] {
        //We know that we want to represent our object as a data type, so we'll specify that
        return [(kUTTypeData as String)]
    }
    func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
        let progress = Progress(totalUnitCount: 100)
        do {
            //Here the object is encoded to a JSON data object and sent to the completion handler
            let data = try JSONEncoder().encode(self)
            progress.completedUnitCount = 100
            completionHandler(data, nil)
        } catch {
            completionHandler(nil, error)
        }
        return progress
    }
    var username: String?
    var avatarImage: String?
    init(username: String, avatarImage: String){
        self.username = username
        self.avatarImage = avatarImage
    }
    override init() {
        super.init()
    }
}
