//
//  ItemCell.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/6/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import SDWebImage
class ItemCell: UITableViewCell {
    
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    var user : UserModel? {
        didSet{
            fullnameLabel.text = user?.username
            avatarImageView.sd_setImage(with: URL(string: user?.avatarImage ?? "https://ra.ac.ae/wp-content/uploads/2017/02/user-icon-placeholder.png"), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
