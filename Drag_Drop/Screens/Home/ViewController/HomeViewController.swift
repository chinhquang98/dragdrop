//
//  HomeViewController.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/6/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import MobileCoreServices

struct My {
    static var cellSnapshot : UIView? = nil
    static var cellIsAnimating : Bool = false
    static var cellNeedToShow : Bool = false
}
struct Path {
    static var initialIndexPath : IndexPath? = nil
}
struct Table{
    static var tag : Int?
}
class HomeViewController: UIViewController {
    var rightUsers = [UserModel]()
    var leftUsers = [UserModel]()
    let imageTag  = 1001
    let textTag = 1000
    
    @IBOutlet weak var rightTableView: UITableView!
    
    @IBOutlet weak var leftTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        rightTableView.register(UINib(nibName: ItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ItemCell.reuseIdentifier)
        leftTableView.register(UINib(nibName: ItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ItemCell.reuseIdentifier)
        rightTableView.delegate = self
        rightTableView.dataSource = self
        leftTableView.delegate = self
        leftTableView.dataSource = self
        
        
        rightUsers.append(UserModel(username: "Harry Potter", avatarImage: "https://timedotcom.files.wordpress.com/2014/07/301386_full1.jpg"))
        rightUsers.append(UserModel(username: "Các Mác", avatarImage: "http://lyluanchinhtri.vn/home/media/k2/items/cache/270346aaee566bd7e522cfe834c2438a_L.jpg"))
        rightUsers.append(UserModel(username: "Harry Potter", avatarImage: "https://timedotcom.files.wordpress.com/2014/07/301386_full1.jpg"))
        rightUsers.append(UserModel(username: "Các Mác", avatarImage: "http://lyluanchinhtri.vn/home/media/k2/items/cache/270346aaee566bd7e522cfe834c2438a_L.jpg"))
        
        leftUsers.append(UserModel(username: "Lenin", avatarImage: "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTIwNjA4NjMzODgyNTEwODYw/vladimir-lenin-9379007-1-402.jpg"))
        leftUsers.append(UserModel(username: "Hiệp sĩ trong tay có kiếm", avatarImage: "https://www.hiepsibaotap.com/wp-content/uploads/2018/11/6_green_ranger-532x405-1.jpg"))
        leftUsers.append(UserModel(username: "Lenin", avatarImage: "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTIwNjA4NjMzODgyNTEwODYw/vladimir-lenin-9379007-1-402.jpg"))
        leftUsers.append(UserModel(username: "Hiệp sĩ trong tay có kiếm", avatarImage: "https://www.hiepsibaotap.com/wp-content/uploads/2018/11/6_green_ranger-532x405-1.jpg"))
        
        
    }
    //MARK: - Calculate the area of a frame
    func calculateArea(of intersection : CGRect) -> CGFloat {
        return intersection.width * intersection.height
    }
    //MARK: - Handle long-press recognizer event
    @objc func didLongPressCell (recognizer: UILongPressGestureRecognizer) {
        
        let cellPP: UIView = recognizer.view!
        /* Get tableview tag and indexpath of the cell whose recognizer is handled
         tag = 1 : Right tableview
         tag = 2 : Left tableview
        */
        Table.tag = cellPP.tag
        let indexPath = Table.tag == 1 ? rightTableView.indexPathForRow(at: recognizer.location(in: rightTableView)) : leftTableView.indexPathForRow(at: recognizer.location(in: leftTableView))
        
        // State of recognizer
        switch recognizer.state {
        //State began
        case .began:
            if Table.tag == 1{
                let cellView = rightTableView.cellForRow(at: indexPath!) as! ItemCell
                Path.initialIndexPath = indexPath
                Table.tag = cellView.tag
                My.cellSnapshot = snapshotOfCell(inputView: cellView)
                var center = cellView.center
                My.cellSnapshot!.alpha = 0.0
                My.cellSnapshot!.center = recognizer.location(in: self.view)
                view.addSubview(My.cellSnapshot!)
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = recognizer.location(in: cellView).y
                    My.cellIsAnimating = true
                    My.cellSnapshot!.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                    My.cellSnapshot!.alpha = 0.98

                }, completion: { (finished) -> Void in
                    if finished {
                        My.cellIsAnimating = false
                        if My.cellNeedToShow {
                            My.cellNeedToShow = false
                        }
                    }
                })
            } else if Table.tag == 2 {
                let cellView = leftTableView.cellForRow(at: indexPath!) as! ItemCell
                Path.initialIndexPath = indexPath
                Table.tag = cellView.tag
                My.cellSnapshot = snapshotOfCell(inputView: cellView)
                var center = cellView.center
                My.cellSnapshot!.alpha = 0.0
                My.cellSnapshot!.center = recognizer.location(in: self.view)
                view.addSubview(My.cellSnapshot!)
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = recognizer.location(in: cellView).y
                    My.cellIsAnimating = true
                    My.cellSnapshot!.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                    My.cellSnapshot!.alpha = 0.98
                    
                }, completion: { (finished) -> Void in
                    if finished {
                        My.cellIsAnimating = false
                        if My.cellNeedToShow {
                            My.cellNeedToShow = false
                        }
                    }
                })
            }
        case .changed:
            if My.cellSnapshot != nil {
                My.cellSnapshot?.center = recognizer.location(in: view)
            }
            
            
        default:

            if Table.tag == 1 {
                let leftIntersection = My.cellSnapshot?.frame.intersection(leftTableView.frame)
                let rightIntersection = My.cellSnapshot?.frame.intersection(rightTableView.frame)
                let areaLeft = calculateArea(of: leftIntersection!)
                let areaRight = calculateArea(of: rightIntersection!)
                let left_index = leftTableView.indexPathForRow(at: recognizer.location(in: leftTableView))
                //print("Init \(Path.initialIndexPath?.row)")
                var destinationIndexPath = IndexPath(row: leftUsers.count - 1 , section: 0)
                if left_index != nil{
                    print("At \(left_index!.row)th of left  ")
                    destinationIndexPath = left_index!
                    let cellContentView = leftTableView.cellForRow(at: left_index!)?.contentView
                    let rect = cellContentView!.convert(cellContentView!.frame, to: self.view) // pass toView nil if you want to convert rect relative to window
                    let point = CGPoint(x: rect.origin.x + rect.size.width / 2, y: rect.origin.y + rect.size.height / 2)
                    
                    let currectY  = My.cellSnapshot?.center.y
                    if currectY! - point.y >= 5.0{
                        
                        destinationIndexPath = IndexPath(row: destinationIndexPath.row + 1, section: 0)
                    }
                    
                    
                    
                    leftUsers.insert(rightUsers[(Path.initialIndexPath?.row)!], at: destinationIndexPath.row)
                    
                    leftTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                    
                    let indexPathArray = leftTableView.indexPathsForVisibleRows
                    if destinationIndexPath.row > (indexPathArray?.last!.row)!{
                        destinationIndexPath = (indexPathArray?.last)!
                    }
                    let cell = leftTableView.cellForRow(at: destinationIndexPath) as! ItemCell
                    if My.cellIsAnimating {
                        My.cellNeedToShow = true
                    } else {
                        cell.isHidden = false
                        cell.alpha = 0.0
                    }
                    UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                        My.cellSnapshot!.center = cell.center
                        My.cellSnapshot!.transform = CGAffineTransform.identity
                        My.cellSnapshot!.alpha = 0.0
                        cell.alpha = 1.0
                        My.cellSnapshot!.removeFromSuperview()
                    }, completion: { (finished) -> Void in
                        if finished {
                            Path.initialIndexPath = nil

                            My.cellSnapshot = nil


                        }
                    })

                } else {
                    if areaLeft > areaRight
                    {
                        let firstIndexPath = IndexPath(row: 0, section: 0)
                        let firstCell = leftTableView.cellForRow(at: firstIndexPath)

                        if (My.cellSnapshot?.frame.intersects(firstCell!.frame))!{
                            print("At first row of left table")
                            //leftUsers.append(rightUsers[(Path.initialIndexPath?.row)!])
                            leftUsers.insert(rightUsers[(Path.initialIndexPath?.row)!], at: 0)
                            destinationIndexPath = IndexPath(row: 0 , section: 0)
                            leftTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                        } else {
                            print("At last row of left table")
                            leftUsers.append(rightUsers[(Path.initialIndexPath?.row)!])
                            destinationIndexPath = IndexPath(row: leftUsers.count - 1 , section: 0)
                            leftTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                        }

                        let cell = leftTableView.cellForRow(at: destinationIndexPath) as! ItemCell
                        if My.cellIsAnimating {
                            My.cellNeedToShow = true
                        } else {
                            cell.isHidden = false
                            cell.alpha = 0.0
                        }
                        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                            My.cellSnapshot!.center = cell.center
                            My.cellSnapshot!.transform = CGAffineTransform.identity
                            My.cellSnapshot!.alpha = 0.0
                            cell.alpha = 1.0
                            My.cellSnapshot!.removeFromSuperview()
                        }, completion: { (finished) -> Void in
                            if finished {
                                Path.initialIndexPath = nil
                                My.cellSnapshot = nil
                            }
                        })
                    } else {
                        print("At right table")
                        if Path.initialIndexPath != nil {
                            let cell = rightTableView.cellForRow(at: Path.initialIndexPath!) as! ItemCell
                            if My.cellIsAnimating {
                                My.cellNeedToShow = true
                            } else {
                                cell.isHidden = false
                                cell.alpha = 0.0
                            }
                            UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                                My.cellSnapshot!.center = cell.center
                                My.cellSnapshot!.transform = CGAffineTransform.identity
                                My.cellSnapshot!.alpha = 0.0
                                cell.alpha = 1.0
                                My.cellSnapshot!.removeFromSuperview()
                            }, completion: { (finished) -> Void in
                                if finished {
                                    Path.initialIndexPath = nil
                                    My.cellSnapshot = nil


                                }
                            })

                        }
                    }
                }
            } else if Table.tag == 2 {
                let leftIntersection = My.cellSnapshot?.frame.intersection(leftTableView.frame)
                let rightIntersection = My.cellSnapshot?.frame.intersection(rightTableView.frame)
                let areaLeft = calculateArea(of: leftIntersection!)
                let areaRight = calculateArea(of: rightIntersection!)
                let right_index = rightTableView.indexPathForRow(at: recognizer.location(in: rightTableView))
                
                var destinationIndexPath = IndexPath(row: rightUsers.count - 1 , section: 0)
                if right_index != nil{
                    print("At \(right_index!.row)th of left  ")
                    
                    // Check where the snapshot we want to drop to
                    
                    destinationIndexPath = right_index!
                    
                    let cellContentView = rightTableView.cellForRow(at: right_index!)?.contentView
                    let rect = cellContentView!.convert(cellContentView!.frame, to: self.view) // pass toView nil if you want to convert rect relative to window
                    let point = CGPoint(x: rect.origin.x + rect.size.width / 2, y: rect.origin.y + rect.size.height / 2)
                    
                    let currectY  = My.cellSnapshot?.center.y
                    if currectY! - point.y >= 5.0{
                        destinationIndexPath = IndexPath(row: destinationIndexPath.row + 1, section: 0)
                    }
                    
                    rightUsers.insert(leftUsers[(Path.initialIndexPath?.row)!], at: destinationIndexPath.row)
                  
                    rightTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                    
                    let indexPathArray = rightTableView.indexPathsForVisibleRows
                    if destinationIndexPath.row > (indexPathArray?.last!.row)!{
                        destinationIndexPath = (indexPathArray?.last)!
                    }
                    let cell = rightTableView.cellForRow(at: destinationIndexPath) as! ItemCell
                    if My.cellIsAnimating {
                        My.cellNeedToShow = true
                    } else {
                        cell.isHidden = false
                        cell.alpha = 0.0
                    }
                    UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                        My.cellSnapshot!.center = cell.center
                        My.cellSnapshot!.transform = CGAffineTransform.identity
                        My.cellSnapshot!.alpha = 0.0
                        cell.alpha = 1.0
                        My.cellSnapshot!.removeFromSuperview()
                    }, completion: { (finished) -> Void in
                        if finished {
                            Path.initialIndexPath = nil
                            
                            My.cellSnapshot = nil
                            
                            
                        }
                    })
                    
                } else {
                    if areaLeft < areaRight
                    {
                        let firstIndexPath = IndexPath(row: 0, section: 0)
                        let firstCell = rightTableView.cellForRow(at: firstIndexPath)
                        
                        if (My.cellSnapshot?.frame.intersects(firstCell!.frame))!{
                            print("At first row of left table")
                            //leftUsers.append(rightUsers[(Path.initialIndexPath?.row)!])
                            rightUsers.insert(leftUsers[(Path.initialIndexPath?.row)!], at: 0)
                            destinationIndexPath = IndexPath(row: 0 , section: 0)
                            rightTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                        } else {
                            print("At last row of left table")
                            rightUsers.append(leftUsers[(Path.initialIndexPath?.row)!])
                            destinationIndexPath = IndexPath(row: rightUsers.count - 1 , section: 0)
                            rightTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                        }
                        
                        let cell = rightTableView.cellForRow(at: destinationIndexPath) as! ItemCell
                        if My.cellIsAnimating {
                            My.cellNeedToShow = true
                        } else {
                            cell.isHidden = false
                            cell.alpha = 0.0
                        }
                        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                            My.cellSnapshot!.center = cell.center
                            My.cellSnapshot!.transform = CGAffineTransform.identity
                            My.cellSnapshot!.alpha = 0.0
                            cell.alpha = 1.0
                            My.cellSnapshot!.removeFromSuperview()
                        }, completion: { (finished) -> Void in
                            if finished {
                                Path.initialIndexPath = nil
                                My.cellSnapshot = nil
                            }
                        })
                    } else {
                        print("At left table")
                        if Path.initialIndexPath != nil {
                            let cell = leftTableView.cellForRow(at: Path.initialIndexPath!) as! ItemCell
                            if My.cellIsAnimating {
                                My.cellNeedToShow = true
                            } else {
                                cell.isHidden = false
                                cell.alpha = 0.0
                            }
                            UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                                My.cellSnapshot!.center = cell.center
                                My.cellSnapshot!.transform = CGAffineTransform.identity
                                My.cellSnapshot!.alpha = 0.0
                                cell.alpha = 1.0
                                My.cellSnapshot!.removeFromSuperview()
                            }, completion: { (finished) -> Void in
                                if finished {
                                    Path.initialIndexPath = nil
                                    My.cellSnapshot = nil
                                }
                            })
                            
                        }
                    }
                }
            }

        }
    }
    //MARK: - Create a snapshot to hold the view of the
    func snapshotOfCell(inputView: UIView) -> UIView {
        
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5, height: 0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    

}
// UITableViewDelegate methods
extension HomeViewController : UITableViewDelegate{
    
}
// UITableViewDatasource methods
extension HomeViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == rightTableView{
            return rightUsers.count
        } else if tableView == leftTableView {
            return leftUsers.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemCell.reuseIdentifier, for: indexPath) as! ItemCell
        
        if tableView == rightTableView {
            
            cell.user = rightUsers[indexPath.row]
            
            cell.contentView.tag = 1
          
        } else {
            cell.user = leftUsers[indexPath.row]
            cell.contentView.tag = 2
        }
        // Set tag for later use
        cell.fullnameLabel.tag = textTag
        cell.avatarImageView.tag = imageTag
        
        //Add long press gesture recognizer to handle drag-drop event
        let lpGestureRecognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(didLongPressCell(recognizer:)))
        cell.contentView.addGestureRecognizer(lpGestureRecognizer)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width / 3
    }
}

